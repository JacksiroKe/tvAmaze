# :movie_camera: tvAmze - Technical Test for Mobile App Developers

This is a mobile application build using [Flutter](https://flutter.dev/) framework using:
- [x] [TvMaze](https://www.tvmaze.com/api) public API. 
- [x] [Back4App platform](https://www.back4app.com) powered by Parse Server
- [x] [Drift](https://pub.dev/packages/drift) Database

## Feautures

- [x] User Authenthication
- [x] Scheduled shows for the day
- [x] Liked shows by user
- [x] Scheduled shows by user
- [x] Show search
- [x] Show view details with episodes

## Download

[Apk Download](release/tvAmaze.apk)

## Screenshot

![Screenshots](release/screenshot.png)



## Set up

### 1. Clone the repo

```bash
$ git clone https://codeberg.org/JacksiroKe/tvAmaze.git
$ cd tvAmaze
```

### 2. Fetching Dependencies

```bash
flutter pub get
```

### 3. Running

```bash
flutter run
```

## Dependency

- [cached_network_image](https://pub.dev/packages/drift)
- [carousel_slider](https://pub.dev/packages/drift)
- [cupertino_icons](https://pub.dev/packages/drift)
- [date_format](https://pub.dev/packages/drift)
- [dio](https://pub.dev/packages/drift)
- [drift](https://pub.dev/packages/drift)
- [flutter_html](https://pub.dev/packages/drift)
- [flutter_keyboard_visibility](https://pub.dev/packages/drift)
- [flutter_secure_storage](https://pub.dev/packages/drift)
- [get_it](https://pub.dev/packages/drift)
- [fluttertoast](https://pub.dev/packages/drift)
- [flutter_localizations](#)
- [icapps_architecture](https://pub.dev/packages/drift)
- [injectable](https://pub.dev/packages/drift)
- [intl](https://pub.dev/packages/drift)
- [json_annotation](https://pub.dev/packages/drift)
- [parse_server_sdk_flutter](https://pub.dev/packages/drift)
- [path](https://pub.dev/packages/drift)
- [path_provider](https://pub.dev/packages/drift)
- [provider](https://pub.dev/packages/drift)
- [retrofit](https://pub.dev/packages/drift)
- [shared_preferences](https://pub.dev/packages/drift)
- [skeleton_loader](https://pub.dev/packages/drift)
- [url_launcher](https://pub.dev/packages/drift)
