import 'package:cached_network_image/cached_network_image.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';

import '../../models/base/episode.dart';
import '../../navigator/mixin/back_navigator.dart';
import '../../navigator/route_names.dart';
import '../../theme/app_colors.dart';
import '../../utils/constants/app_constants.dart';
import '../../view_models/show_viewmodel.dart';
import '../../widgets/general/episode_item.dart';
import '../../widgets/general/no_data_to_show.dart';
import '../../widgets/general/tag_view.dart';
import '../../widgets/progress/list_loading.dart';
import '../../widgets/provider/provider_widget.dart';

/// Show screen
class ShowScreen extends StatefulWidget {
  static const String routeName = RouteNames.showScreen;

  const ShowScreen({
    Key? key,
  }) : super(key: key);

  @override
  ShowScreenState createState() => ShowScreenState();
}

@visibleForTesting
class ShowScreenState extends State<ShowScreen> with BackNavigatorMixin {
  ShowViewModel? vm;
  Size? size;

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<ShowViewModel>(
      create: () => GetIt.I()..init(),
      consumerWithThemeAndLocalization:
          (context, viewModel, child, theme, localization) {
        vm = viewModel;
        return screenWidget(context);
      },
    );
  }

  Widget screenWidget(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: AppColors.accent,
      appBar: AppBar(
        title: Text(vm!.showName!),
        actions: <Widget>[
          IconButton(
            icon: Icon(vm!.likeIcon),
            onPressed: () => vm!.likeShow(),
          ),
          IconButton(
            icon: const Icon(Icons.schedule),
            onPressed: () => selectDate(context),
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          height: size!.height,
          padding: const EdgeInsets.only(top: 10),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, AppColors.accent, Colors.black],
            ),
          ),
          child: mainContainer(context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => vm!.watchNow(vm!.show!.url!),
        child: const Icon(Icons.play_arrow),
      ),
    );
  }

  Widget mainContainer(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: vm!.isBusy
            ? const ListLoading()
            : vm!.show != null
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      topContainer(),
                      summaryContainer(),
                      const SizedBox(height: 5),
                      listContainer(context),
                    ],
                  )
                : const NoDataToShow(
                    title: AppConstants.itsEmptyHere,
                    description: AppConstants.itsEmptyHereBody,
                  ),
      ),
    );
  }

  Widget topContainer() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Image(
                image: CachedNetworkImageProvider(vm!.show!.image!.medium!),
                fit: BoxFit.cover,
                height: 200,
              ),
            ),
          ),
          Container(
            width: size!.width - 165,
            height: 200,
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  vm!.show!.name!,
                  style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  '${vm!.show!.schedule!.days![0]} ${vm!.show!.schedule!.time!}',
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.blue,
                  ),
                ),
                Row(
                  children: <Widget>[
                    vm!.show!.network != null
                        ? Text(
                            vm!.show!.network!.name!,
                            style: const TextStyle(
                              fontSize: 20,
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        : Container(),
                    const SizedBox(width: 5),
                    Text(
                      vm!.show!.language!.toUpperCase(),
                      style: const TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 5),
                const Divider(color: Colors.black, height: 5),
                SizedBox(
                  height: 35,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: const EdgeInsets.all(5),
                    itemBuilder: (context, index) {
                      return TagView(
                          tagText: vm!.show!.genres![index],
                          height: size!.height);
                    },
                    itemCount: vm!.show!.genres!.length,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget summaryContainer() {
    return vm!.show!.summary != null
        ? Container(
            margin: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Html(
              data: vm!.show!.summary,
              shrinkWrap: true,
            ),
          )
        : Container();
  }

  Widget listContainer(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(5),
      physics: const ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: vm!.episodes!.length,
      itemBuilder: (BuildContext context, int index) {
        final Episode episode = vm!.episodes![index];
        return EpisodeItem(
          episode: episode,
          size: size!,
          onTap: () => vm!.watchNow(episode.url!),
        );
      },
    );
  }

  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: vm!.selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null) {
      setState(() {
        vm!.selectedDate = picked;
        vm!.dateController.text = DateFormat.yMd().format(vm!.selectedDate);
      });
      await selectTime(context);
    }
  }

  Future<void> selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: vm!.selectedTime,
    );
    if (picked != null) {
      setState(() {
        vm!.selectedTime = picked;
        vm!.hour = vm!.selectedTime.hour.toString();
        vm!.minute = vm!.selectedTime.minute.toString();
        vm!.time = '${vm!.hour} : ${vm!.minute}';
        vm!.timeController.text = vm!.time!;
        vm!.timeController.text = formatDate(
          DateTime(2019, 08, 1, vm!.selectedTime.hour, vm!.selectedTime.minute),
          [hh, ':', nn, " ", am],
        ).toString();
      });
      await vm!.scheduleShow();
    }
  }
}
