import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../../navigator/main_navigator.dart';
import '../../navigator/mixin/back_navigator.dart';
import '../../navigator/route_names.dart';
import '../../theme/app_colors.dart';
import '../../utils/constants/app_constants.dart';
import '../../view_models/login_viewmodel.dart';
import '../../widgets/action/submit_button.dart';
import '../../widgets/inputs/form_input.dart';
import '../../widgets/inputs/password_input.dart';
import '../../widgets/progress/circular_progress.dart';
import '../../widgets/provider/provider_widget.dart';

/// Login screen
class LoginScreen extends StatefulWidget {
  static const String routeName = RouteNames.loginScreen;

  const LoginScreen({
    Key? key,
  }) : super(key: key);

  @override
  LoginScreenState createState() => LoginScreenState();
}

@visibleForTesting
class LoginScreenState extends State<LoginScreen>
    with BackNavigatorMixin
    implements LoginNavigator {
  LoginViewModel? vm;

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<LoginViewModel>(
      create: () => GetIt.I()..init(this),
      consumerWithThemeAndLocalization:
          (context, viewModel, child, theme, localization) {
        vm = viewModel;
        return Scaffold(
          appBar: AppBar(
            title: const Text(AppConstants.loginScreenTitle),
          ),
          body: mainContainer(),
        );
      },
    );
  }

  Widget mainContainer() {
    return vm!.isBusy
        ? const CircularProgress()
        : ListView(
            padding: const EdgeInsets.all(5),
            children: [
              FormInput(
                iLabel: 'Username',
                iController: vm!.usernameController!,
                prefix: const Icon(Icons.person),
              ),
              PasswordInput(
                iLabel: 'Password',
                iController: vm!.passwordController!,
              ),
              SubmitButton(
                title: 'Login',
                onPressed: () => vm!.loginUser(),
              ),
              const SizedBox(height: 20),
              const Center(
                child: Text("Don't have an account?"),
              ),
              SubmitButton(
                title: 'Sign Up',
                onPressed: () => MainNavigatorWidget.of(context).goToSignup(),
              ),
            ],
          );
  }

  @override
  void goToHome() => MainNavigatorWidget.of(context).goToHome();
}
