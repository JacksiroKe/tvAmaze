import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tvamaze/widgets/action/submit_button.dart';
import 'package:tvamaze/widgets/progress/circular_progress.dart';

import '../../navigator/main_navigator.dart';
import '../../navigator/mixin/back_navigator.dart';
import '../../navigator/route_names.dart';
import '../../theme/app_colors.dart';
import '../../utils/constants/app_constants.dart';
import '../../view_models/signup_viewmodel.dart';
import '../../widgets/inputs/form_input.dart';
import '../../widgets/inputs/password_input.dart';
import '../../widgets/provider/provider_widget.dart';

/// Signup screen
class SignupScreen extends StatefulWidget {
  static const String routeName = RouteNames.signupScreen;

  const SignupScreen({
    Key? key,
  }) : super(key: key);

  @override
  SignupScreenState createState() => SignupScreenState();
}

@visibleForTesting
class SignupScreenState extends State<SignupScreen>
    with BackNavigatorMixin
    implements SignupNavigator {
  SignupViewModel? vm;

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<SignupViewModel>(
      create: () => GetIt.I()..init(this),
      consumerWithThemeAndLocalization:
          (context, viewModel, child, theme, localization) {
        vm = viewModel;
        return Scaffold(
          appBar: AppBar(
            title: const Text(AppConstants.signupScreenTitle),
          ),
          body: mainContainer(),
        );
      },
    );
  }

  Widget mainContainer() {
    return vm!.isBusy
        ? const CircularProgress()
        : ListView(
            padding: const EdgeInsets.all(5),
            children: [
              FormInput(
                iLabel: 'Username',
                iController: vm!.usernameController!,
                prefix: const Icon(Icons.person),
              ),
              FormInput(
                iLabel: 'Email Address',
                iController: vm!.emailController!,
                prefix: const Icon(Icons.email),
              ),
              PasswordInput(
                iController: vm!.passwordController!,
                iType: TextInputType.phone,
              ),
              SubmitButton(
                title: 'Sign Up',
                onPressed: () => vm!.signupUser(),
              ),
              const SizedBox(height: 20),
              const Center(
                child: Text("Already have an account?"),
              ),
              SubmitButton(
                title: 'Login',
                onPressed: () => MainNavigatorWidget.of(context).goToLogin(),
              ),
            ],
          );
  }

  @override
  void goToHome() => MainNavigatorWidget.of(context).goToHome();
}
