part of 'home_screen.dart';

/// Search tab
class SearchTab extends StatelessWidget {
  final HomeViewModel? vm;
  SearchTab({Key? key, required this.vm}) : super(key: key);
  Size? size;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Container(
          height: size!.height,
          padding: const EdgeInsets.only(top: 10),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, AppColors.accent, Colors.black],
            ),
          ),
          child: bodyContainer(context),
        ),
      ),
    );
  }

  Widget bodyContainer(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: vm!.isBusy
            ? const ListLoading()
            : vm!.shows!.isNotEmpty
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      topContainer(context),
                      listContainer(context),
                    ],
                  )
                : const NoDataToShow(
                    title: AppConstants.itsEmptyHere,
                    description: AppConstants.itsEmptyHereBody,
                  ),
      ),
    );
  }

  Widget topContainer(BuildContext context) {
    return SizedBox(
      height: size!.height * 0.3,
      child: CarouselSlider(
        options: CarouselOptions(
          aspectRatio: 2.0,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          initialPage: 2,
          autoPlay: true,
        ),
        items: vm!.shows!.map((show) {
          return Builder(
            builder: (BuildContext context) {
              return CarouselItem(
                show: show,
                size: size!,
                onTap: () => vm!.viewShow(show),
              );
            },
          );
        }).toList(),
      ),
    );
  }

  Widget listContainer(BuildContext context) {
    return SizedBox(
      child: ListView.builder(
        padding: const EdgeInsets.all(5),
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: vm!.filtered!.length,
        itemBuilder: (BuildContext context, int index) {
          final Show show = vm!.filtered![index];
          return ShowItem(
            show: show,
            size: size!,
            onTap: () => vm!.viewShow(show),
          );
        },
      ),
    );
  }
}
