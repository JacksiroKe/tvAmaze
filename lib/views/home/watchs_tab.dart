part of 'home_screen.dart';

/// Watch tab
class WatchsTab extends StatelessWidget {
  final HomeViewModel? vm;
  WatchsTab({Key? key, required this.vm}) : super(key: key);
  Size? size;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
      body: SafeArea(
        child: Container(
          height: size!.height,
          padding: const EdgeInsets.only(top: 10),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.white, AppColors.accent, Colors.black],
            ),
          ),
          child: listContainer(context),
        ),
      ),
    );
  }

  Widget listContainer(BuildContext context) {
    return SizedBox(
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: vm!.watchs!.length,
        itemBuilder: (BuildContext context, int index) {
          final Watch watch = vm!.watchs![index];
          final Show show = Show(id: watch.show);
          return WatchItem(
            watch: watch,
            size: size!,
            onTap: () => vm!.viewShow(show),
          );
        },
      ),
    );
  }
}
