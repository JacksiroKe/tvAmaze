import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import '../../models/base/show.dart';
import '../../models/db_models/like.dart';
import '../../models/db_models/watch.dart';
import '../../navigator/main_navigator.dart';
import '../../navigator/route_names.dart';
import '../../theme/app_colors.dart';
import '../../utils/constants/app_constants.dart';
import '../../utils/search_bar_delegate.dart';
import '../../view_models/home_viewmodel.dart';
import '../../widgets/general/carousel_item.dart';
import '../../widgets/general/like_item.dart';
import '../../widgets/general/no_data_to_show.dart';
import '../../widgets/general/show_item.dart';
import '../../widgets/general/watch_item.dart';
import '../../widgets/progress/list_loading.dart';
import '../../widgets/provider/provider_widget.dart';

part 'search_tab.dart';
part 'likes_tab.dart';
part 'watchs_tab.dart';

/// Timed Welcome screen
class HomeScreen extends StatefulWidget {
  static const String routeName = RouteNames.homeScreen;

  const HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}

@visibleForTesting
class HomeScreenState extends State<HomeScreen> implements HomeNavigator {
  HomeViewModel? vm;
  int currentIndex = 0;

  void updateIndex(int value) {
    setState(() {
      currentIndex = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ProviderWidget<HomeViewModel>(
      create: () => GetIt.I()..init(this),
      consumerWithThemeAndLocalization:
          (context, viewModel, child, theme, localization) {
        vm = viewModel;
        return screenWidget(context);
      },
    );
  }

  Widget screenWidget(BuildContext context) {
    List screens = [
      SearchTab(vm: vm!),
      LikesTab(vm: vm!),
      WatchsTab(vm: vm!),
    ];

    return Scaffold(
      appBar: AppBar(
        title: const Text(AppConstants.appTitle),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () => showSearch(
              context: context,
              delegate: SearchBarDelegate(context, vm!),
            ),
          ),
          IconButton(
            icon: const Icon(Icons.logout),
            onPressed: () => vm!.logoutUser(),
          ),
        ],
      ),
      body: screens[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: updateIndex,
        selectedItemColor: Colors.blue[700],
        selectedFontSize: 13,
        unselectedFontSize: 13,
        iconSize: 30,
        items: const [
          BottomNavigationBarItem(
            label: "Home",
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            label: "Likes",
            icon: Icon(Icons.favorite),
          ),
          BottomNavigationBarItem(
            label: "Schedule",
            icon: Icon(Icons.tv),
          ),
        ],
      ),
    );
  }

  @override
  void goToShow() => MainNavigatorWidget.of(context).goToShow();

  @override
  void restratApp() => MainNavigatorWidget.of(context).restratApp();
}
