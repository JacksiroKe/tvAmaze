class AppFonts {
  AppFonts._();

  static const appFonts = 'TrebuchetMS';

  static const title = appFonts;
  static const body = appFonts;
  static const button = appFonts;
}
