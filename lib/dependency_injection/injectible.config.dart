// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i25;
import 'package:drift/drift.dart' as _i4;
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:icapps_architecture/icapps_architecture.dart' as _i3;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i9;

import '../database/dao/likes_dao_storage.dart' as _i12;
import '../database/dao/watchs_dao_storage.dart' as _i11;
import '../database/tvamaze_db.dart' as _i10;
import '../repository/database_repository.dart' as _i14;
import '../repository/debug_repository.dart' as _i15;
import '../repository/locale_repository.dart' as _i17;
import '../repository/refresh_repository.dart' as _i20;
import '../repository/secure_storage/auth_storage.dart' as _i13;
import '../repository/secure_storage/secure_storage.dart' as _i8;
import '../repository/shared_prefs/local_storage.dart' as _i16;
import '../repository/web_repository.dart' as _i28;
import '../utils/interceptor/network_auth_interceptor.dart' as _i19;
import '../utils/interceptor/network_error_interceptor.dart' as _i6;
import '../utils/interceptor/network_log_interceptor.dart' as _i7;
import '../utils/interceptor/network_refresh_interceptor.dart' as _i24;
import '../view_models/global_viewmodel.dart' as _i23;
import '../view_models/home_viewmodel.dart' as _i29;
import '../view_models/login_viewmodel.dart' as _i18;
import '../view_models/show_viewmodel.dart' as _i30;
import '../view_models/signup_viewmodel.dart' as _i21;
import '../view_models/splash_viewmodel.dart' as _i22;
import '../webservice/api_client.dart' as _i27;
import '../webservice/web_service.dart' as _i26;
import 'injectible.dart' as _i31;

const String _dev = 'dev';
const String _prod = 'prod';
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  final registerModule = _$RegisterModule();
  gh.singleton<_i3.ConnectivityHelper>(registerModule.connectivityHelper());
  await gh.singletonAsync<_i4.DatabaseConnection>(
    () => registerModule.provideDatabaseConnection(),
    preResolve: true,
  );
  gh.lazySingleton<_i5.FlutterSecureStorage>(() => registerModule.storage());
  gh.singleton<_i6.NetworkErrorInterceptor>(
      _i6.NetworkErrorInterceptor(get<_i3.ConnectivityHelper>()));
  gh.singleton<_i7.NetworkLogInterceptor>(_i7.NetworkLogInterceptor());
  gh.lazySingleton<_i8.SecureStorage>(
      () => _i8.SecureStorage(get<_i5.FlutterSecureStorage>()));
  await gh.singletonAsync<_i9.SharedPreferences>(
    () => registerModule.prefs(),
    preResolve: true,
  );
  gh.lazySingleton<_i10.TvAmazeDb>(
      () => registerModule.provideTvAmazeDb(get<_i4.DatabaseConnection>()));
  gh.lazySingleton<_i11.WatchsDaoStorage>(
      () => _i11.WatchsDaoStorage(get<_i10.TvAmazeDb>()));
  gh.lazySingleton<_i12.LikesDaoStorage>(
      () => _i12.LikesDaoStorage(get<_i10.TvAmazeDb>()));
  gh.lazySingleton<_i3.SharedPreferenceStorage>(
      () => registerModule.sharedPreferences(get<_i9.SharedPreferences>()));
  gh.lazySingleton<_i3.SimpleKeyValueStorage>(
      () => registerModule.keyValueStorage(
            get<_i3.SharedPreferenceStorage>(),
            get<_i8.SecureStorage>(),
          ));
  gh.lazySingleton<_i13.AuthStorage>(
      () => _i13.AuthStorage(get<_i3.SimpleKeyValueStorage>()));
  gh.lazySingleton<_i14.DatabaseRepository>(() => _i14.DatabaseRepository(
        get<_i12.LikesDaoStorage>(),
        get<_i11.WatchsDaoStorage>(),
      ));
  gh.lazySingleton<_i15.DebugRepository>(
      () => _i15.DebugRepository(get<_i3.SharedPreferenceStorage>()));
  gh.lazySingleton<_i16.LocalStorage>(() => _i16.LocalStorage(
        get<_i13.AuthStorage>(),
        get<_i3.SharedPreferenceStorage>(),
      ));
  gh.lazySingleton<_i17.LocaleRepository>(
      () => _i17.LocaleRepository(get<_i3.SharedPreferenceStorage>()));
  gh.factory<_i18.LoginViewModel>(
      () => _i18.LoginViewModel(get<_i16.LocalStorage>()));
  gh.singleton<_i19.NetworkAuthInterceptor>(
      _i19.NetworkAuthInterceptor(get<_i13.AuthStorage>()));
  gh.lazySingleton<_i20.RefreshRepository>(
      () => _i20.RefreshRepository(get<_i13.AuthStorage>()));
  gh.factory<_i21.SignupViewModel>(
      () => _i21.SignupViewModel(get<_i16.LocalStorage>()));
  gh.factory<_i22.SplashViewModel>(
      () => _i22.SplashViewModel(get<_i16.LocalStorage>()));
  gh.singleton<_i23.GlobalViewModel>(_i23.GlobalViewModel(
    get<_i17.LocaleRepository>(),
    get<_i15.DebugRepository>(),
    get<_i16.LocalStorage>(),
  ));
  gh.singleton<_i24.NetworkRefreshInterceptor>(
      _i24.NetworkRefreshInterceptor(get<_i20.RefreshRepository>()));
  gh.lazySingleton<_i3.CombiningSmartInterceptor>(
      () => registerModule.provideCombiningSmartInterceptor(
            get<_i7.NetworkLogInterceptor>(),
            get<_i19.NetworkAuthInterceptor>(),
            get<_i6.NetworkErrorInterceptor>(),
            get<_i24.NetworkRefreshInterceptor>(),
          ));
  gh.lazySingleton<_i25.Dio>(
      () => registerModule.provideDio(get<_i3.CombiningSmartInterceptor>()));
  gh.singleton<_i26.WebService>(
    _i27.ApiClient(get<_i25.Dio>()),
    registerFor: {
      _dev,
      _prod,
    },
  );
  gh.lazySingleton<_i28.WebRepository>(
      () => _i28.WebRepository(get<_i26.WebService>()));
  gh.factory<_i29.HomeViewModel>(() => _i29.HomeViewModel(
        get<_i28.WebRepository>(),
        get<_i14.DatabaseRepository>(),
        get<_i16.LocalStorage>(),
      ));
  gh.factory<_i30.ShowViewModel>(() => _i30.ShowViewModel(
        get<_i28.WebRepository>(),
        get<_i14.DatabaseRepository>(),
        get<_i16.LocalStorage>(),
      ));
  return get;
}

class _$RegisterModule extends _i31.RegisterModule {}
