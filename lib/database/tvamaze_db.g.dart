// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tvamaze_db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: type=lint
class Likes extends DataClass implements Insertable<Likes> {
  final int id;
  final int show;
  final String name;
  final String day;
  final String time;
  final String url;
  final String image;
  final String summary;
  final DateTime createdAt;
  final DateTime updatedAt;
  Likes(
      {required this.id,
      required this.show,
      required this.name,
      required this.day,
      required this.time,
      required this.url,
      required this.image,
      required this.summary,
      required this.createdAt,
      required this.updatedAt});
  factory Likes.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Likes(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      show: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}show'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      day: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}day'])!,
      time: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}time'])!,
      url: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}url'])!,
      image: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image'])!,
      summary: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}summary'])!,
      createdAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at'])!,
      updatedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['show'] = Variable<int>(show);
    map['name'] = Variable<String>(name);
    map['day'] = Variable<String>(day);
    map['time'] = Variable<String>(time);
    map['url'] = Variable<String>(url);
    map['image'] = Variable<String>(image);
    map['summary'] = Variable<String>(summary);
    map['created_at'] = Variable<DateTime>(createdAt);
    map['updated_at'] = Variable<DateTime>(updatedAt);
    return map;
  }

  LikesTableCompanion toCompanion(bool nullToAbsent) {
    return LikesTableCompanion(
      id: Value(id),
      show: Value(show),
      name: Value(name),
      day: Value(day),
      time: Value(time),
      url: Value(url),
      image: Value(image),
      summary: Value(summary),
      createdAt: Value(createdAt),
      updatedAt: Value(updatedAt),
    );
  }

  factory Likes.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Likes(
      id: serializer.fromJson<int>(json['id']),
      show: serializer.fromJson<int>(json['show']),
      name: serializer.fromJson<String>(json['name']),
      day: serializer.fromJson<String>(json['day']),
      time: serializer.fromJson<String>(json['time']),
      url: serializer.fromJson<String>(json['url']),
      image: serializer.fromJson<String>(json['image']),
      summary: serializer.fromJson<String>(json['summary']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'show': serializer.toJson<int>(show),
      'name': serializer.toJson<String>(name),
      'day': serializer.toJson<String>(day),
      'time': serializer.toJson<String>(time),
      'url': serializer.toJson<String>(url),
      'image': serializer.toJson<String>(image),
      'summary': serializer.toJson<String>(summary),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Likes copyWith(
          {int? id,
          int? show,
          String? name,
          String? day,
          String? time,
          String? url,
          String? image,
          String? summary,
          DateTime? createdAt,
          DateTime? updatedAt}) =>
      Likes(
        id: id ?? this.id,
        show: show ?? this.show,
        name: name ?? this.name,
        day: day ?? this.day,
        time: time ?? this.time,
        url: url ?? this.url,
        image: image ?? this.image,
        summary: summary ?? this.summary,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Likes(')
          ..write('id: $id, ')
          ..write('show: $show, ')
          ..write('name: $name, ')
          ..write('day: $day, ')
          ..write('time: $time, ')
          ..write('url: $url, ')
          ..write('image: $image, ')
          ..write('summary: $summary, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id, show, name, day, time, url, image, summary, createdAt, updatedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Likes &&
          other.id == this.id &&
          other.show == this.show &&
          other.name == this.name &&
          other.day == this.day &&
          other.time == this.time &&
          other.url == this.url &&
          other.image == this.image &&
          other.summary == this.summary &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class LikesTableCompanion extends UpdateCompanion<Likes> {
  final Value<int> id;
  final Value<int> show;
  final Value<String> name;
  final Value<String> day;
  final Value<String> time;
  final Value<String> url;
  final Value<String> image;
  final Value<String> summary;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const LikesTableCompanion({
    this.id = const Value.absent(),
    this.show = const Value.absent(),
    this.name = const Value.absent(),
    this.day = const Value.absent(),
    this.time = const Value.absent(),
    this.url = const Value.absent(),
    this.image = const Value.absent(),
    this.summary = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  LikesTableCompanion.insert({
    this.id = const Value.absent(),
    this.show = const Value.absent(),
    this.name = const Value.absent(),
    this.day = const Value.absent(),
    this.time = const Value.absent(),
    this.url = const Value.absent(),
    this.image = const Value.absent(),
    this.summary = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  static Insertable<Likes> custom({
    Expression<int>? id,
    Expression<int>? show,
    Expression<String>? name,
    Expression<String>? day,
    Expression<String>? time,
    Expression<String>? url,
    Expression<String>? image,
    Expression<String>? summary,
    Expression<DateTime>? createdAt,
    Expression<DateTime>? updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (show != null) 'show': show,
      if (name != null) 'name': name,
      if (day != null) 'day': day,
      if (time != null) 'time': time,
      if (url != null) 'url': url,
      if (image != null) 'image': image,
      if (summary != null) 'summary': summary,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  LikesTableCompanion copyWith(
      {Value<int>? id,
      Value<int>? show,
      Value<String>? name,
      Value<String>? day,
      Value<String>? time,
      Value<String>? url,
      Value<String>? image,
      Value<String>? summary,
      Value<DateTime>? createdAt,
      Value<DateTime>? updatedAt}) {
    return LikesTableCompanion(
      id: id ?? this.id,
      show: show ?? this.show,
      name: name ?? this.name,
      day: day ?? this.day,
      time: time ?? this.time,
      url: url ?? this.url,
      image: image ?? this.image,
      summary: summary ?? this.summary,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (show.present) {
      map['show'] = Variable<int>(show.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (day.present) {
      map['day'] = Variable<String>(day.value);
    }
    if (time.present) {
      map['time'] = Variable<String>(time.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (summary.present) {
      map['summary'] = Variable<String>(summary.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LikesTableCompanion(')
          ..write('id: $id, ')
          ..write('show: $show, ')
          ..write('name: $name, ')
          ..write('day: $day, ')
          ..write('time: $time, ')
          ..write('url: $url, ')
          ..write('image: $image, ')
          ..write('summary: $summary, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $LikesTableTable extends LikesTable
    with TableInfo<$LikesTableTable, Likes> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LikesTableTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _showMeta = const VerificationMeta('show');
  @override
  late final GeneratedColumn<int?> show = GeneratedColumn<int?>(
      'show', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _dayMeta = const VerificationMeta('day');
  @override
  late final GeneratedColumn<String?> day = GeneratedColumn<String?>(
      'day', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _timeMeta = const VerificationMeta('time');
  @override
  late final GeneratedColumn<String?> time = GeneratedColumn<String?>(
      'time', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _urlMeta = const VerificationMeta('url');
  @override
  late final GeneratedColumn<String?> url = GeneratedColumn<String?>(
      'url', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String?> image = GeneratedColumn<String?>(
      'image', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _summaryMeta = const VerificationMeta('summary');
  @override
  late final GeneratedColumn<String?> summary = GeneratedColumn<String?>(
      'summary', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime?> createdAt = GeneratedColumn<DateTime?>(
      'created_at', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: Constant(DateTime.now()));
  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime?> updatedAt = GeneratedColumn<DateTime?>(
      'updated_at', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: Constant(DateTime.now()));
  @override
  List<GeneratedColumn> get $columns =>
      [id, show, name, day, time, url, image, summary, createdAt, updatedAt];
  @override
  String get aliasedName => _alias ?? 'likes_table';
  @override
  String get actualTableName => 'likes_table';
  @override
  VerificationContext validateIntegrity(Insertable<Likes> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('show')) {
      context.handle(
          _showMeta, show.isAcceptableOrUnknown(data['show']!, _showMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    if (data.containsKey('day')) {
      context.handle(
          _dayMeta, day.isAcceptableOrUnknown(data['day']!, _dayMeta));
    }
    if (data.containsKey('time')) {
      context.handle(
          _timeMeta, time.isAcceptableOrUnknown(data['time']!, _timeMeta));
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url']!, _urlMeta));
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    if (data.containsKey('summary')) {
      context.handle(_summaryMeta,
          summary.isAcceptableOrUnknown(data['summary']!, _summaryMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Likes map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Likes.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $LikesTableTable createAlias(String alias) {
    return $LikesTableTable(attachedDatabase, alias);
  }
}

class Watchs extends DataClass implements Insertable<Watchs> {
  final int id;
  final int show;
  final String name;
  final String day;
  final String time;
  final String url;
  final String image;
  final String summary;
  final String schedule;
  final DateTime createdAt;
  final DateTime updatedAt;
  Watchs(
      {required this.id,
      required this.show,
      required this.name,
      required this.day,
      required this.time,
      required this.url,
      required this.image,
      required this.summary,
      required this.schedule,
      required this.createdAt,
      required this.updatedAt});
  factory Watchs.fromData(Map<String, dynamic> data, {String? prefix}) {
    final effectivePrefix = prefix ?? '';
    return Watchs(
      id: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}id'])!,
      show: const IntType()
          .mapFromDatabaseResponse(data['${effectivePrefix}show'])!,
      name: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}name'])!,
      day: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}day'])!,
      time: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}time'])!,
      url: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}url'])!,
      image: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}image'])!,
      summary: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}summary'])!,
      schedule: const StringType()
          .mapFromDatabaseResponse(data['${effectivePrefix}schedule'])!,
      createdAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at'])!,
      updatedAt: const DateTimeType()
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at'])!,
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['show'] = Variable<int>(show);
    map['name'] = Variable<String>(name);
    map['day'] = Variable<String>(day);
    map['time'] = Variable<String>(time);
    map['url'] = Variable<String>(url);
    map['image'] = Variable<String>(image);
    map['summary'] = Variable<String>(summary);
    map['schedule'] = Variable<String>(schedule);
    map['created_at'] = Variable<DateTime>(createdAt);
    map['updated_at'] = Variable<DateTime>(updatedAt);
    return map;
  }

  WatchsTableCompanion toCompanion(bool nullToAbsent) {
    return WatchsTableCompanion(
      id: Value(id),
      show: Value(show),
      name: Value(name),
      day: Value(day),
      time: Value(time),
      url: Value(url),
      image: Value(image),
      summary: Value(summary),
      schedule: Value(schedule),
      createdAt: Value(createdAt),
      updatedAt: Value(updatedAt),
    );
  }

  factory Watchs.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Watchs(
      id: serializer.fromJson<int>(json['id']),
      show: serializer.fromJson<int>(json['show']),
      name: serializer.fromJson<String>(json['name']),
      day: serializer.fromJson<String>(json['day']),
      time: serializer.fromJson<String>(json['time']),
      url: serializer.fromJson<String>(json['url']),
      image: serializer.fromJson<String>(json['image']),
      summary: serializer.fromJson<String>(json['summary']),
      schedule: serializer.fromJson<String>(json['schedule']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'show': serializer.toJson<int>(show),
      'name': serializer.toJson<String>(name),
      'day': serializer.toJson<String>(day),
      'time': serializer.toJson<String>(time),
      'url': serializer.toJson<String>(url),
      'image': serializer.toJson<String>(image),
      'summary': serializer.toJson<String>(summary),
      'schedule': serializer.toJson<String>(schedule),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Watchs copyWith(
          {int? id,
          int? show,
          String? name,
          String? day,
          String? time,
          String? url,
          String? image,
          String? summary,
          String? schedule,
          DateTime? createdAt,
          DateTime? updatedAt}) =>
      Watchs(
        id: id ?? this.id,
        show: show ?? this.show,
        name: name ?? this.name,
        day: day ?? this.day,
        time: time ?? this.time,
        url: url ?? this.url,
        image: image ?? this.image,
        summary: summary ?? this.summary,
        schedule: schedule ?? this.schedule,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Watchs(')
          ..write('id: $id, ')
          ..write('show: $show, ')
          ..write('name: $name, ')
          ..write('day: $day, ')
          ..write('time: $time, ')
          ..write('url: $url, ')
          ..write('image: $image, ')
          ..write('summary: $summary, ')
          ..write('schedule: $schedule, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, show, name, day, time, url, image,
      summary, schedule, createdAt, updatedAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Watchs &&
          other.id == this.id &&
          other.show == this.show &&
          other.name == this.name &&
          other.day == this.day &&
          other.time == this.time &&
          other.url == this.url &&
          other.image == this.image &&
          other.summary == this.summary &&
          other.schedule == this.schedule &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class WatchsTableCompanion extends UpdateCompanion<Watchs> {
  final Value<int> id;
  final Value<int> show;
  final Value<String> name;
  final Value<String> day;
  final Value<String> time;
  final Value<String> url;
  final Value<String> image;
  final Value<String> summary;
  final Value<String> schedule;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const WatchsTableCompanion({
    this.id = const Value.absent(),
    this.show = const Value.absent(),
    this.name = const Value.absent(),
    this.day = const Value.absent(),
    this.time = const Value.absent(),
    this.url = const Value.absent(),
    this.image = const Value.absent(),
    this.summary = const Value.absent(),
    this.schedule = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  WatchsTableCompanion.insert({
    this.id = const Value.absent(),
    this.show = const Value.absent(),
    this.name = const Value.absent(),
    this.day = const Value.absent(),
    this.time = const Value.absent(),
    this.url = const Value.absent(),
    this.image = const Value.absent(),
    this.summary = const Value.absent(),
    this.schedule = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  static Insertable<Watchs> custom({
    Expression<int>? id,
    Expression<int>? show,
    Expression<String>? name,
    Expression<String>? day,
    Expression<String>? time,
    Expression<String>? url,
    Expression<String>? image,
    Expression<String>? summary,
    Expression<String>? schedule,
    Expression<DateTime>? createdAt,
    Expression<DateTime>? updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (show != null) 'show': show,
      if (name != null) 'name': name,
      if (day != null) 'day': day,
      if (time != null) 'time': time,
      if (url != null) 'url': url,
      if (image != null) 'image': image,
      if (summary != null) 'summary': summary,
      if (schedule != null) 'schedule': schedule,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  WatchsTableCompanion copyWith(
      {Value<int>? id,
      Value<int>? show,
      Value<String>? name,
      Value<String>? day,
      Value<String>? time,
      Value<String>? url,
      Value<String>? image,
      Value<String>? summary,
      Value<String>? schedule,
      Value<DateTime>? createdAt,
      Value<DateTime>? updatedAt}) {
    return WatchsTableCompanion(
      id: id ?? this.id,
      show: show ?? this.show,
      name: name ?? this.name,
      day: day ?? this.day,
      time: time ?? this.time,
      url: url ?? this.url,
      image: image ?? this.image,
      summary: summary ?? this.summary,
      schedule: schedule ?? this.schedule,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (show.present) {
      map['show'] = Variable<int>(show.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (day.present) {
      map['day'] = Variable<String>(day.value);
    }
    if (time.present) {
      map['time'] = Variable<String>(time.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (image.present) {
      map['image'] = Variable<String>(image.value);
    }
    if (summary.present) {
      map['summary'] = Variable<String>(summary.value);
    }
    if (schedule.present) {
      map['schedule'] = Variable<String>(schedule.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('WatchsTableCompanion(')
          ..write('id: $id, ')
          ..write('show: $show, ')
          ..write('name: $name, ')
          ..write('day: $day, ')
          ..write('time: $time, ')
          ..write('url: $url, ')
          ..write('image: $image, ')
          ..write('summary: $summary, ')
          ..write('schedule: $schedule, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $WatchsTableTable extends WatchsTable
    with TableInfo<$WatchsTableTable, Watchs> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $WatchsTableTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int?> id = GeneratedColumn<int?>(
      'id', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _showMeta = const VerificationMeta('show');
  @override
  late final GeneratedColumn<int?> show = GeneratedColumn<int?>(
      'show', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(0));
  final VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String?> name = GeneratedColumn<String?>(
      'name', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _dayMeta = const VerificationMeta('day');
  @override
  late final GeneratedColumn<String?> day = GeneratedColumn<String?>(
      'day', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _timeMeta = const VerificationMeta('time');
  @override
  late final GeneratedColumn<String?> time = GeneratedColumn<String?>(
      'time', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _urlMeta = const VerificationMeta('url');
  @override
  late final GeneratedColumn<String?> url = GeneratedColumn<String?>(
      'url', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _imageMeta = const VerificationMeta('image');
  @override
  late final GeneratedColumn<String?> image = GeneratedColumn<String?>(
      'image', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _summaryMeta = const VerificationMeta('summary');
  @override
  late final GeneratedColumn<String?> summary = GeneratedColumn<String?>(
      'summary', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _scheduleMeta = const VerificationMeta('schedule');
  @override
  late final GeneratedColumn<String?> schedule = GeneratedColumn<String?>(
      'schedule', aliasedName, false,
      type: const StringType(),
      requiredDuringInsert: false,
      defaultValue: const Constant(''));
  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime?> createdAt = GeneratedColumn<DateTime?>(
      'created_at', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: Constant(DateTime.now()));
  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  @override
  late final GeneratedColumn<DateTime?> updatedAt = GeneratedColumn<DateTime?>(
      'updated_at', aliasedName, false,
      type: const IntType(),
      requiredDuringInsert: false,
      defaultValue: Constant(DateTime.now()));
  @override
  List<GeneratedColumn> get $columns => [
        id,
        show,
        name,
        day,
        time,
        url,
        image,
        summary,
        schedule,
        createdAt,
        updatedAt
      ];
  @override
  String get aliasedName => _alias ?? 'watchs_table';
  @override
  String get actualTableName => 'watchs_table';
  @override
  VerificationContext validateIntegrity(Insertable<Watchs> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('show')) {
      context.handle(
          _showMeta, show.isAcceptableOrUnknown(data['show']!, _showMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    }
    if (data.containsKey('day')) {
      context.handle(
          _dayMeta, day.isAcceptableOrUnknown(data['day']!, _dayMeta));
    }
    if (data.containsKey('time')) {
      context.handle(
          _timeMeta, time.isAcceptableOrUnknown(data['time']!, _timeMeta));
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url']!, _urlMeta));
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image']!, _imageMeta));
    }
    if (data.containsKey('summary')) {
      context.handle(_summaryMeta,
          summary.isAcceptableOrUnknown(data['summary']!, _summaryMeta));
    }
    if (data.containsKey('schedule')) {
      context.handle(_scheduleMeta,
          schedule.isAcceptableOrUnknown(data['schedule']!, _scheduleMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at']!, _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Watchs map(Map<String, dynamic> data, {String? tablePrefix}) {
    return Watchs.fromData(data,
        prefix: tablePrefix != null ? '$tablePrefix.' : null);
  }

  @override
  $WatchsTableTable createAlias(String alias) {
    return $WatchsTableTable(attachedDatabase, alias);
  }
}

abstract class _$TvAmazeDb extends GeneratedDatabase {
  _$TvAmazeDb(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  _$TvAmazeDb.connect(DatabaseConnection c) : super.connect(c);
  late final $LikesTableTable likesTable = $LikesTableTable(this);
  late final $WatchsTableTable watchsTable = $WatchsTableTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [likesTable, watchsTable];
}
