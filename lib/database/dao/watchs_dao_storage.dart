import 'package:drift/drift.dart';
import 'package:injectable/injectable.dart';

import '../../models/db_models/watch.dart';
import '../../models/db_tables/watchs_table.dart';
import '../tvamaze_db.dart';

part 'watchs_dao_storage.g.dart';

@lazySingleton
abstract class WatchsDaoStorage {
  @factoryMethod
  factory WatchsDaoStorage(TvAmazeDb db) = WatchsDao;

  Future<List<Watch>> getAllWatchs();
  Future<void> createWatch(Watch watch);
  Future<void> updateWatch(Watch watch);
  Future<void> deleteWatch(Watch watch);
  Future<void> deleteWatchs();
}

@DriftAccessor(tables: [
  WatchsTable,
])
class WatchsDao extends DatabaseAccessor<TvAmazeDb>
    with _$WatchsDaoMixin
    implements WatchsDaoStorage {
  WatchsDao(TvAmazeDb db) : super(db);

  @override
  Future<List<Watch>> getAllWatchs() async {
    final Stream<List<Watch>> streams = customSelect(
      'SELECT * FROM ${db.watchsTable.actualTableName} '
      'ORDER BY ${db.watchsTable.id.name} ASC;',
      readsFrom: {db.watchsTable},
    ).watch().map(
      (rows) {
        final List<Watch> results = [];
        for (final row in rows) {
          results.add(
            Watch(
              id: const IntType().mapFromDatabaseResponse(row.data['id'])!,
              show: const IntType().mapFromDatabaseResponse(row.data['show'])!,
              url: const StringType().mapFromDatabaseResponse(row.data['url'])!,
              image: const StringType()
                  .mapFromDatabaseResponse(row.data['image'])!,
              summary: const StringType()
                  .mapFromDatabaseResponse(row.data['summary'])!,
              schedule:
                  const StringType().mapFromDatabaseResponse(row.data['schedule'])!,
              createdAt: const DateTimeType()
                  .mapFromDatabaseResponse(row.data['created_at'])!,
              updatedAt: const DateTimeType()
                  .mapFromDatabaseResponse(row.data['updated_at'])!,
            ),
          );
        }
        return results;
      },
    );
    return await streams.first;
  }

  @override
  Future<void> createWatch(Watch watch) => into(db.watchsTable).insert(
        WatchsTableCompanion.insert(
          show: Value(watch.show!),
          url: Value(watch.url!),
          image: Value(watch.image!),
          summary: Value(watch.summary!),
          schedule: Value(watch.schedule!),
          createdAt: Value(watch.createdAt!),
        ),
      );

  @override
  Future<void> updateWatch(Watch watch) =>
      (update(db.watchsTable)..where((row) => row.id.equals(watch.id))).write(
        WatchsTableCompanion(
          show: Value(watch.show!),
          url: Value(watch.url!),
          image: Value(watch.image!),
          summary: Value(watch.summary!),
          schedule: Value(watch.schedule!),
          updatedAt: Value(watch.updatedAt!),
        ),
      );

  @override
  Future<void> deleteWatch(Watch watch) =>
      (delete(db.watchsTable)..where((row) => row.id.equals(watch.id))).go();

  @override
  Future<void> deleteWatchs() => (delete(db.watchsTable)).go();
}
