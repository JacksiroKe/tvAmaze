import 'package:drift/drift.dart';
import 'package:injectable/injectable.dart';

import '../../models/db_models/like.dart';
import '../../models/db_tables/likes_table.dart';
import '../tvamaze_db.dart';

part 'likes_dao_storage.g.dart';

@lazySingleton
abstract class LikesDaoStorage {
  @factoryMethod
  factory LikesDaoStorage(TvAmazeDb db) = LikesDao;

  Future<List<Like>> getAllLikes();
  Future<void> createLike(Like like);
  Future<void> updateLike(Like like);
  Future<void> deleteLike(Like like);
  Future<void> deleteLikes();
}

@DriftAccessor(tables: [
  LikesTable,
])
class LikesDao extends DatabaseAccessor<TvAmazeDb>
    with _$LikesDaoMixin
    implements LikesDaoStorage {
  LikesDao(TvAmazeDb db) : super(db);

  @override
  Future<List<Like>> getAllLikes() async {
    final Stream<List<Like>> streams = customSelect(
      'SELECT * FROM ${db.likesTable.actualTableName} '
      'ORDER BY ${db.likesTable.id.name} ASC;',
      readsFrom: {db.likesTable},
    ).watch().map(
      (rows) {
        final List<Like> results = [];
        for (final row in rows) {
          results.add(
            Like(
              id: const IntType().mapFromDatabaseResponse(row.data['id'])!,
              show: const IntType().mapFromDatabaseResponse(row.data['show'])!,
              url: const StringType().mapFromDatabaseResponse(row.data['url'])!,
              image: const StringType()
                  .mapFromDatabaseResponse(row.data['image'])!,
              summary: const StringType()
                  .mapFromDatabaseResponse(row.data['summary'])!,
              createdAt: const DateTimeType()
                  .mapFromDatabaseResponse(row.data['created_at'])!,
              updatedAt: const DateTimeType()
                  .mapFromDatabaseResponse(row.data['updated_at'])!,
            ),
          );
        }
        return results;
      },
    );
    return await streams.first;
  }

  @override
  Future<void> createLike(Like like) => into(db.likesTable).insert(
        LikesTableCompanion.insert(
          show: Value(like.show!),
          url: Value(like.url!),
          image: Value(like.image!),
          summary: Value(like.summary!),
          createdAt: Value(like.createdAt!),
        ),
      );

  @override
  Future<void> updateLike(Like like) =>
      (update(db.likesTable)..where((row) => row.id.equals(like.id))).write(
        LikesTableCompanion(
          show: Value(like.show!),
          url: Value(like.url!),
          image: Value(like.image!),
          summary: Value(like.summary!),
          updatedAt: Value(like.updatedAt!),
        ),
      );

  @override
  Future<void> deleteLike(Like like) =>
      (delete(db.likesTable)..where((row) => row.id.equals(like.id))).go();

  @override
  Future<void> deleteLikes() => (delete(db.likesTable)).go();
}
