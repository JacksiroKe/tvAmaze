import 'package:drift/drift.dart';

import '../models/db_tables/likes_table.dart';
import '../models/db_tables/watchs_table.dart';

part 'tvamaze_db.g.dart';

@DriftDatabase(tables: [
  LikesTable,
  WatchsTable,
])
class TvAmazeDb extends _$TvAmazeDb {
  TvAmazeDb(QueryExecutor db) : super(db);

  TvAmazeDb.connect(DatabaseConnection connection)
      : super.connect(connection);

  @override
  int get schemaVersion => 1;

  Future<void> deleteAllData() {
    return transaction(() async {
      for (final table in allTables) {
        await delete<Table, dynamic>(table).go();
      }
    });
  }
}
