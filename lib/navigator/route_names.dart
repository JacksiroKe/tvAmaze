class RouteNames {
  RouteNames._();
  static const splashScreen = 'splash';
  static const loginScreen = 'login';
  static const signupScreen = 'signup';
  static const homeScreen = 'home';
  static const showScreen = 'show';
}
