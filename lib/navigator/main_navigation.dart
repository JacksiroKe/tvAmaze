import 'package:flutter/material.dart';

/// Abstract class for the major navigation functions for the app
abstract class MainNavigation {
  void goBack<T>({T? result});

  void restratApp();
  void goToSplash();
  void goToLogin();
  void goToSignup();
  void goToShow();
  void goToHome();

  void showCustomDialog<T>({required WidgetBuilder builder});
}

mixin MainNavigationMixin<T extends StatefulWidget> on State<T>
    implements MainNavigation {}
