import '../base/show.dart';

class ShowSearchResp {
  double? score;
  Show? show;

  ShowSearchResp({this.score, this.show});

  ShowSearchResp.fromJson(Map<String, dynamic> json) {
    score = json['score'];
    show = json['show'] != null ? Show.fromJson(json['show']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['score'] = score;
    if (show != null) {
      data['show'] = show!.toJson();
    }
    return data;
  }
}