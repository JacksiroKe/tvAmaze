class Watch {
  int? id;
  int? show;
  String? name;
  String? day;
  String? time;
  String? url;
  String? image;
  String? summary;
  String? schedule;
  DateTime? createdAt;
  DateTime? updatedAt;

  Watch({
    this.id,
    this.show,
    this.name,
    this.day,
    this.time,
    this.url,
    this.image,
    this.summary,
    this.schedule,
    this.createdAt,
    this.updatedAt,
  });

  Watch.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    show = json['show'];
    name = json['name'];
    day = json['day'];
    time = json['time'];
    url = json['url'];
    image = json['image'];
    summary = json['summary'];
    schedule = json['schedule'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['show'] = show;
    data['name'] = name;
    data['day'] = day;
    data['time'] = time;
    data['url'] = url;
    data['image'] = image;
    data['summary'] = summary;
    data['schedule'] = schedule;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    return data;
  }
}
