class Like {
  int? id;
  int? show;
  String? name;
  String? day;
  String? time;
  String? url;
  String? image;
  String? summary;
  DateTime? createdAt;
  DateTime? updatedAt;

  Like({
    this.id,
    this.show,
    this.name,
    this.day,
    this.time,
    this.url,
    this.image,
    this.summary,
    this.createdAt,
    this.updatedAt,
  });

  Like.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    show = json['show'];
    name = json['name'];
    day = json['day'];
    time = json['time'];
    url = json['url'];
    image = json['image'];
    summary = json['summary'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['show'] = show;
    data['name'] = name;
    data['day'] = day;
    data['time'] = time;
    data['url'] = url;
    data['image'] = image;
    data['summary'] = summary;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    return data;
  }
}
