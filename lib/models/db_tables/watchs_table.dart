import 'package:drift/drift.dart';

import '../../database/tvamaze_db.dart';
import '../db_models/watch.dart';

@DataClassName('Watchs')
class WatchsTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get show => integer().withDefault(const Constant(0))();
  TextColumn get name => text().withDefault(const Constant(''))();
  TextColumn get day => text().withDefault(const Constant(''))();
  TextColumn get time => text().withDefault(const Constant(''))();
  TextColumn get url => text().withDefault(const Constant(''))();
  TextColumn get image => text().withDefault(const Constant(''))();
  TextColumn get summary => text().withDefault(const Constant(''))();
  TextColumn get schedule => text().withDefault(const Constant(''))();
  DateTimeColumn get createdAt => dateTime().withDefault(Constant(DateTime.now()))();
  DateTimeColumn get updatedAt => dateTime().withDefault(Constant(DateTime.now()))();
}

extension WatchsExtension on Watchs {
  Watch getModel() => Watch(
        id: id,
        show: show,
        name: name,
        day: day,
        time: time,
        url: url,
        image: image,
        summary: summary,
        schedule: schedule,
        createdAt: createdAt,
        updatedAt: updatedAt,
      );
}

extension WatchExtension on Watch {
  WatchsTableCompanion getDbModel() {
    final id = this.id;
    return WatchsTableCompanion.insert(
      id: id == null ? const Value.absent() : Value(id),
      show: Value(show!),
      name: Value(name!),
      day: Value(day!),
      time: Value(time!),
      url: Value(url!),
      image: Value(image!),
      summary: Value(summary!),
      schedule: Value(schedule!),
      createdAt: Value(createdAt!),
      updatedAt: Value(updatedAt!),
    );
  }
}
