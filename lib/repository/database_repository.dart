import 'package:injectable/injectable.dart';

import '../database/dao/likes_dao_storage.dart';
import '../database/dao/watchs_dao_storage.dart';
import '../models/db_models/like.dart';
import '../models/db_models/watch.dart';

@lazySingleton
abstract class DatabaseRepository {
  @factoryMethod
  factory DatabaseRepository(
    LikesDaoStorage likeDao,
    WatchsDaoStorage watchDao,
  ) = DbRepo;

  Future<List<Like>> getAllLikes();
  Future<void> createLike(Like like);
  Future<void> updateLike(Like like);
  Future<void> deleteLike(Like like);
  Future<void> deleteLikes();

  Future<List<Watch>> getAllWatchs();
  Future<void> createWatch(Watch watch);
  Future<void> updateWatch(Watch watch);
  Future<void> deleteWatch(Watch watch);
  Future<void> deleteWatchs();
}

class DbRepo implements DatabaseRepository {
  final LikesDaoStorage likesDao;
  final WatchsDaoStorage watchsDao;

  DbRepo(this.likesDao, this.watchsDao);

  @override
  Future<List<Like>> getAllLikes() async {
    return await likesDao.getAllLikes();
  }

  @override
  Future<void> createLike(Like like) async {
    return await likesDao.createLike(like);
  }

  @override
  Future<void> updateLike(Like like) async {
    return await likesDao.updateLike(like);
  }

  @override
  Future<void> deleteLike(Like like) async {
    return await likesDao.deleteLike(like);
  }

  @override
  Future<void> deleteLikes() async {
    return await likesDao.deleteLikes();
  }

  @override
  Future<List<Watch>> getAllWatchs() async {
    return await watchsDao.getAllWatchs();
  }

  @override
  Future<void> createWatch(Watch watch) async {
    return await watchsDao.createWatch(watch);
  }

  @override
  Future<void> updateWatch(Watch watch) async {
    return await watchsDao.updateWatch(watch);
  }

  @override
  Future<void> deleteWatch(Watch watch) async {
    return await watchsDao.deleteWatch(watch);
  }

  @override
  Future<void> deleteWatchs() async {
    return await watchsDao.deleteWatchs();
  }

}
