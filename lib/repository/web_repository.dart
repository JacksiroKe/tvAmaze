import 'package:injectable/injectable.dart';

import '../models/base/episode.dart';
import '../models/base/show.dart';
import '../models/response/schedule_resp.dart';
import '../models/response/show_search_resp.dart';
import '../webservice/web_service.dart';

/// Web Repository to provide an instance of webservices
@lazySingleton
abstract class WebRepository {
  @factoryMethod
  factory WebRepository(WebService webService) = WebRepo;

  /// The schedule is a complete list of episodes that air in a given country on a given date. 
  /// Episodes are returned in the order in which they are aired, and full information about the episode and the corresponding show is included.
  Future<List<ScheduleResp>> getSchedule();

  /// Search through all the shows in our database by the show's name. A fuzzy algorithm is used (with a fuzziness value of 2), 
  /// meaning that shows will be found even if your query contains small typos. Results are returned in order of relevancy 
  /// (best matches on top) and contain each show's full information
  Future<List<ShowSearchResp>> getShowSearch(String query);

  /// Retrieve all primary information for a given show
  Future<Show> getShow(int show);

  /// A complete list of episodes for the given show. Episodes are returned in their airing order, and include full episode information
  Future<List<Episode>> getShowEpisodes(int show);
}

class WebRepo implements WebRepository {
  final WebService webService;

  WebRepo(this.webService);

  @override
  Future<List<ScheduleResp>> getSchedule() async {
    final response = await webService.getSchedule();
    return response;
  }

  @override
  Future<List<ShowSearchResp>> getShowSearch(String query) async {
    final response = await webService.getShowSearch(query);
    return response;
  }

  @override
  Future<Show> getShow(int show) async {
    final response = await webService.getShow(show);
    return response;
  }

  @override
  Future<List<Episode>> getShowEpisodes(int show) async {
    final response = await webService.getShowEpisodes(show);
    return response;
  }
}