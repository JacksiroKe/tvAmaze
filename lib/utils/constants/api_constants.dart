class ApiConstants {
  static const basicAuthPrefixHeader = 'Basic';
  static const protectedAuthenticationHeaderPrefix = 'Bearer';
  static const authorizationHeader = 'Authorization';

  //Parse api constants
  static const parseApiUrl = 'https://parseapi.back4app.com';
  static const applicationID = 'pfMKWtX9hrdGmmUtQ5XotWKN76KazT58O0ZEANnZ';
  static const clientKey = 'cg5PGBLKgDHn7BOhy12wzxAB7KCffqrcMrraY7WI';

  //TvMaze api constants
  static const tvmazeApiUrl = 'https://api.tvmaze.com/';
  static const schedule = 'schedule';
  static const showSearch = 'search/shows';
  static const shows = 'shows/';
  static const episodes = '/episodes';
}
