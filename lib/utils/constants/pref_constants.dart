class PrefConstants {
  static const appearanceThemeKey = 'appearanceTheme';
  static const userEmail = 'emailaddress';
  static const userName = 'username';
  static const passWord = 'password';
}