class AppConstants {
  AppConstants._();

  static const appTitle = "tvAmaze";

  static const loginScreenTitle = "Login to your Account";
  static const signupScreenTitle = "Sign Up to your Account";

  static const appIcon = "assets/images/app_icon.png";

  static const itsEmptyHere = "Wueh! It's empty here";
  static const itsEmptyHereBody = "All caught here, reload once again";

  static const showLiked = "has been liked";
  static const showScheduled = " scheduled for ";
  static const showDisliked = "is no longer liked";
}
