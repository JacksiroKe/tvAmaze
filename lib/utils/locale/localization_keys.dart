//============================================================//
//THIS FILE IS AUTO GENERATED. DO NOT EDIT//
//============================================================//
class LocalizationKeys {

  /// Translations:
  ///
  /// en:  **'continue'**
  ///
  /// fr:  **'continuer'**
  static const continueButton = 'continueButton';

  /// Translations:
  ///
  /// en:  **'career'**
  ///
  /// fr:  **''**
  static const careerButton = 'careerButton';

}
