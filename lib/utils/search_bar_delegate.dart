import 'package:flutter/material.dart';

import '../models/base/show.dart';
import '../view_models/home_viewmodel.dart';
import '../widgets/general/show_item.dart';

class SearchBarDelegate extends SearchDelegate<List> {
  HomeViewModel vm;
  Size? size;

  SearchBarDelegate(BuildContext context, this.vm);

  @override
  ThemeData appBarTheme(BuildContext context) {
    return super.appBarTheme(context);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () => query = "",
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        query = '';
        showSuggestions(context);
      },
    );
  }

  /// Function triggered when "ENTER" is pressed.
  @override
  Widget buildResults(BuildContext context) {
    if (query.isNotEmpty) vm.searchNow(query);
    return buildItems(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    if (query.isNotEmpty) vm.searchNow(query);
    return buildItems(context);
  }

  Widget buildItems(BuildContext context) {
    size = MediaQuery.of(context).size;

    return SizedBox(
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: vm.results!.length,
        itemBuilder: (BuildContext context, int index) {
          final Show show = vm.results![index];
          return ShowItem(
            show: show,
            size: size!,
            onTap: () => vm.viewShow(show),
          );
        },
      ),
    );
  }
}
