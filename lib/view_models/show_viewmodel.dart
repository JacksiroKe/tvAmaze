import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:icapps_architecture/icapps_architecture.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/base/episode.dart';
import '../models/base/show.dart';
import '../models/db_models/like.dart';
import '../models/db_models/watch.dart';
import '../repository/database_repository.dart';
import '../repository/shared_prefs/local_storage.dart';
import '../repository/web_repository.dart';
import '../utils/constants/app_constants.dart';
import '../widgets/general/toast.dart';

@injectable
class ShowViewModel with ChangeNotifierEx {
  final WebRepository api;
  final DatabaseRepository dbase;
  final LocalStorage localStorage;

  ShowViewModel(this.api, this.dbase, this.localStorage);

  bool isBusy = false, isLiked = false;

  int? showid;
  Show? show;
  Like? like;
  Watch? watch;
  List<Episode>? episodes;
  IconData likeIcon = Icons.favorite_border;

  String? setTime, setDate;
  String? hour, minute, time;
  String? dateTime, showName = AppConstants.appTitle;
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = const TimeOfDay(hour: 00, minute: 00);

  TextEditingController dateController = TextEditingController();
  TextEditingController timeController = TextEditingController();

  Future<void> init() async {
    showid = localStorage.show!.id;
    dateController.text = DateFormat.yMd().format(
      DateTime.now(),
    );
    timeController.text = formatDate(
        DateTime(
          2022,
          11,
          29,
          DateTime.now().hour,
          DateTime.now().minute,
        ),
        [hh, ':', nn, " ", am]).toString();

    fetchData();
  }

  Future<void> fetchData() async {
    isBusy = true;
    notifyListeners();

    show = await api.getShow(showid!);
    episodes = await api.getShowEpisodes(showid!);
    showName = show!.name!;
    like = Like(
      image: show!.image!.medium!,
      summary: show!.summary,
      url: show!.url,
      name: show!.name,
      day: show!.schedule!.days![0],
      time: show!.schedule!.time!,
    );

    watch = Watch(
      image: show!.image!.medium!,
      summary: show!.summary,
      url: show!.url,
      name: show!.name,
      day: show!.schedule!.days![0],
      time: show!.schedule!.time!,
    );

    isBusy = false;
    notifyListeners();
  }

  /// Add a show to liked shows
  Future<void> likeShow() async {
    isLiked = !isLiked;

    likeIcon = isLiked ? Icons.favorite : Icons.favorite_border;
    if (isLiked) {
      await dbase.createLike(like!);
      showToast(
        text: '${show!.name} ${AppConstants.showLiked}',
        state: ToastStates.success,
      );
    } else {
      await dbase.deleteLike(like!);
      showToast(
        text: '${show!.name} ${AppConstants.showDisliked}',
        state: ToastStates.success,
      );
    }
    notifyListeners();
  }

  /// Add a show to liked shows
  Future<void> scheduleShow() async {
    if (isLiked) {
      watch!.schedule = '${dateController.text} ${timeController.text}';
      await dbase.createWatch(watch!);
      showToast(
        text: '${show!.name} ${AppConstants.showScheduled} ${watch!.schedule}',
        state: ToastStates.success,
      );
    }
    notifyListeners();
  }

  Future<void> watchNow(String link) async {
    final Uri url = Uri.parse(link);
    if (await canLaunchUrl(url)) await launchUrl(url);
  }

  Future<void> favoriteShow() async {}
}
