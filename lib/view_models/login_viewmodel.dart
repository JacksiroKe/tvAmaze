import 'package:flutter/material.dart';
import 'package:icapps_architecture/icapps_architecture.dart';
import 'package:injectable/injectable.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

import '../../repository/shared_prefs/local_storage.dart';
import '../utils/constants/pref_constants.dart';
import '../widgets/general/toast.dart';

@injectable
class LoginViewModel with ChangeNotifierEx {
  final LocalStorage localStorage;
  late final LoginNavigator navigator;

  bool isBusy = false;
  LoginViewModel(this.localStorage);

  TextEditingController? usernameController, passwordController;

  Future<void> init(LoginNavigator screenNavigator) async {
    navigator = screenNavigator;
    usernameController = TextEditingController(text: localStorage.getPrefString(PrefConstants.userName));
    passwordController = TextEditingController(text: localStorage.getPrefString(PrefConstants.passWord));
  }

  Future<void> loginUser() async {
    isBusy = true;
    notifyListeners();

    final user = ParseUser(
      usernameController!.text.trim(),
      passwordController!.text.trim(),
      null,
    );

    var response = await user.login();

    localStorage.setPrefString(
        PrefConstants.userName, usernameController!.text.trim());
    localStorage.setPrefString(
        PrefConstants.passWord, passwordController!.text.trim());

    isBusy = false;
    notifyListeners();

    if (response.success) {
      showToast(text: 'Login was successful', state: ToastStates.success);
      navigator.goToHome();
    } else {
      showToast(text: response.error!.message, state: ToastStates.error);
    }
  }
}

abstract class LoginNavigator {
  void goToHome();
}
