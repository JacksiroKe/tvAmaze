import 'package:icapps_architecture/icapps_architecture.dart';
import 'package:injectable/injectable.dart';

import '../repository/shared_prefs/local_storage.dart';

@injectable
class SplashViewModel with ChangeNotifierEx {
  final LocalStorage localStorage;
  late final SplashNavigator navigator;

  SplashViewModel(this.localStorage);

  Future<void> init(SplashNavigator screenNavigator) async {
    navigator = screenNavigator;

    await Future.delayed(const Duration(seconds: 3), () {});

    navigator.goToLogin();
  }
}

abstract class SplashNavigator {
  void goToLogin();
  void goToHome();
}
