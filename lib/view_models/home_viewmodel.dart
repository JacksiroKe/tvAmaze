import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:icapps_architecture/icapps_architecture.dart';
import 'package:injectable/injectable.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';
import 'package:tvamaze/models/base/schedule.dart';

import '../models/base/show.dart';
import '../models/db_models/like.dart';
import '../models/db_models/watch.dart';
import '../models/response/schedule_resp.dart';
import '../models/response/show_search_resp.dart';
import '../repository/database_repository.dart';
import '../repository/shared_prefs/local_storage.dart';
import '../repository/web_repository.dart';
import '../utils/constants/pref_constants.dart';
import '../widgets/general/carousel_item.dart';
import '../widgets/general/show_item.dart';
import '../widgets/general/toast.dart';

@injectable
class HomeViewModel with ChangeNotifierEx {
  final WebRepository api;
  final DatabaseRepository dbase;
  final LocalStorage localStorage;
  late final HomeNavigator navigator;

  HomeViewModel(this.api, this.dbase, this.localStorage);

  bool isBusy = false;
  List<Like>? likes = [];
  List<Watch>? watchs = [];

  List<Show>? shows = [], filtered = [], results = [];
  List<ScheduleResp>? schedules = [];
  List<ShowSearchResp>? searches = [];
  BuildContext? context;
  Size? size;

  Future<void> init(HomeNavigator screenNavigator) async {
    navigator = screenNavigator;
    await fetchData();
  }

  Future<void> searchNow(String query) async {
    isBusy = true;
    notifyListeners();

    likes = await dbase.getAllLikes();
    watchs = await dbase.getAllWatchs();
    searches = await api.getShowSearch(query);

    for (int i = 0; i < searches!.length; i++) {
      results!.add(searches![i].show!);
    }

    isBusy = false;
    notifyListeners();
  }

  Future<void> fetchData() async {
    isBusy = true;
    notifyListeners();

    schedules = await api.getSchedule();

    for (int i = 0; i < schedules!.length; i++) {
      shows!.add(schedules![i].show!);
    }

    for (int i = 0; i < 5; i++) {
      filtered!.add(schedules![i].show!);
    }

    isBusy = false;
    notifyListeners();
  }

  Widget topContainer() {
    return SizedBox(
      height: 300,
      child: CarouselSlider(
        options: CarouselOptions(
          aspectRatio: 2.0,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          initialPage: 2,
          autoPlay: true,
        ),
        items: shows!.map((show) {
          return Builder(
            builder: (BuildContext context) {
              return CarouselItem(
                show: show,
                size: size!,
                onTap: () => viewShow(show),
              );
            },
          );
        }).toList(),
      ),
    );
  }

  Widget listContainer() {
    return Container(
      height: size!.height * 0.7,
      padding: const EdgeInsets.only(right: 2),
      child: Scrollbar(
        thickness: 10,
        radius: const Radius.circular(20),
        child: ListView.builder(
          itemCount: shows!.length,
          padding: EdgeInsets.only(
            left: size!.height * 0.0082,
            right: size!.height * 0.0082,
          ),
          itemBuilder: (context, index) {
            final Show show = shows![index];
            return ShowItem(
              show: show,
              size: size!,
              onTap: () => viewShow(show),
            );
          },
        ),
      ),
    );
  }

  void viewShow(Show? show) async {
    localStorage.show = show;
    navigator.goToShow();
  }

  Future<void> logoutUser() async {
    isBusy = true;
    notifyListeners();

    final user = ParseUser(
      localStorage.getPrefString(PrefConstants.userName),
      localStorage.getPrefString(PrefConstants.passWord),
      null,
    );

    await user.logout();
    showToast(
      text: 'You have been logged out.',
      state: ToastStates.success,
    );
    navigator.restratApp();
  }
}

abstract class HomeNavigator {
  void goToShow();
  void restratApp();
}
