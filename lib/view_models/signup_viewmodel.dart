import 'package:flutter/material.dart';
import 'package:icapps_architecture/icapps_architecture.dart';
import 'package:injectable/injectable.dart';
import 'package:parse_server_sdk_flutter/parse_server_sdk.dart';

import '../../repository/shared_prefs/local_storage.dart';
import '../utils/constants/pref_constants.dart';
import '../widgets/general/toast.dart';

@injectable
class SignupViewModel with ChangeNotifierEx {
  final LocalStorage localStorage;
  late final SignupNavigator navigator;

  bool isBusy = false;
  SignupViewModel(this.localStorage);

  TextEditingController? usernameController,
      emailController,
      passwordController;

  Future<void> init(SignupNavigator screenNavigator) async {
    navigator = screenNavigator;
    emailController = TextEditingController(text: localStorage.getPrefString(PrefConstants.userEmail));
    usernameController = TextEditingController(text: localStorage.getPrefString(PrefConstants.userName));
    passwordController = TextEditingController(text: localStorage.getPrefString(PrefConstants.passWord));
  }

  Future<void> signupUser() async {
    isBusy = true;
    notifyListeners();

    final user = ParseUser.createUser(
      usernameController!.text.trim(),
      passwordController!.text.trim(),
      emailController!.text.trim(),
    );

    var response = await user.signUp();

    localStorage.setPrefString(
        PrefConstants.userEmail, emailController!.text.trim());
    localStorage.setPrefString(
        PrefConstants.userName, usernameController!.text.trim());
    localStorage.setPrefString(
        PrefConstants.passWord, passwordController!.text.trim());

    isBusy = false;
    notifyListeners();

    if (response.success) {
      showToast(text: 'Signup was successful', state: ToastStates.success);
      navigator.goToHome();
    } else {
      showToast(text: response.error!.message, state: ToastStates.error);
    }
  }

}

abstract class SignupNavigator {
  void goToHome();
}
