import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../models/db_models/watch.dart';

class WatchItem extends StatelessWidget {
  final Watch watch;
  final Size size;
  final Function()? onTap;

  const WatchItem({
    Key? key,
    required this.watch,
    required this.size,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'watchIndex_${watch.id}',
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          margin: EdgeInsets.only(bottom: size.height * 0.0049),
          child: Padding(
            padding: EdgeInsets.all(size.height * 0.0049),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image(
                      image: CachedNetworkImageProvider(watch.image!),
                      fit: BoxFit.cover,
                      height: size.height * 0.3,
                    ),
                  ),
                ),
                Container(
                  width: size.width - 150,
                  height: size.height * 0.3,
                  padding: const EdgeInsets.all(5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        watch.name!,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          const Text(
                            'SCHEDULE: ',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.blue,
                            ),
                          ),
                          Text(
                            watch.schedule!,
                            style: const TextStyle(
                              fontSize: 16,
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      watch.summary != null
                          ? SizedBox(
                              height: 80,
                              child: Html(
                                data: watch.summary,
                                shrinkWrap: true,
                              ),
                            )
                          : Container(),
                      const SizedBox(height: 5),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
