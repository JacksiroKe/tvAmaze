import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../models/base/show.dart';
import 'tag_view.dart';

class ShowItem extends StatelessWidget {
  final Show show;
  final Size size;
  final Function()? onTap;

  const ShowItem({
    Key? key,
    required this.show,
    required this.size,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'showIndex_${show.id}',
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          margin: EdgeInsets.only(bottom: size.height * 0.0049),
          child: Padding(
            padding: EdgeInsets.all(size.height * 0.0049),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Image(
                      image: CachedNetworkImageProvider(show.image!.medium!),
                      fit: BoxFit.cover,
                      height: size.height * 0.3,
                    ),
                  ),
                ),
                Container(
                  width: size.width - 175,
                  height: size.height * 0.3,
                  padding: const EdgeInsets.all(5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        show.name!,
                        maxLines: 1,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            '${show.schedule!.days![0]} ${show.schedule!.time!}',
                            style: const TextStyle(
                              fontSize: 16,
                              color: Colors.blue,
                            ),
                          ),
                          show.network != null
                              ? const Text(
                                  ' on ',
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          show.network != null
                              ? Text(
                                  show.network!.name!,
                                  style: const TextStyle(
                                    fontSize: 16,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : Container(),
                          const SizedBox(width: 5),
                          Text(
                            show.language!.toUpperCase(),
                            style: const TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                      show.summary != null
                          ? SizedBox(
                              height: 80,
                              child: Html(
                                data: show.summary,
                                shrinkWrap: true,
                              ),
                            )
                          : Container(),
                      const SizedBox(height: 5),
                      const Divider(color: Colors.black, height: 5),
                      SizedBox(
                        height: 35,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          padding: const EdgeInsets.all(5),
                          itemBuilder: (context, index) {
                            return TagView(
                                tagText: show.genres![index],
                                height: size.height);
                          },
                          itemCount: show.genres!.length,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
