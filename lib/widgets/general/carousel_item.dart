import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../../models/base/show.dart';
import 'tag_view.dart';

class CarouselItem extends StatelessWidget {
  final Show show;
  final Size size;
  final Function()? onTap;

  const CarouselItem({
    Key? key,
    required this.show,
    required this.size,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'CarouselIndex_${show.id}',
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          color: Colors.black,
          child: Padding(
            padding: EdgeInsets.all(size.height * 0.0049),
            child: Container(
              width: size.height * 0.4,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(show.image!.medium!),
                  fit: BoxFit.cover,
                  opacity: 0.3,
                ),
                color: Colors.black,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Image(
                  image: CachedNetworkImageProvider(show.image!.medium!),
                  fit: BoxFit.cover,
                  height: size.height * 0.3,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
