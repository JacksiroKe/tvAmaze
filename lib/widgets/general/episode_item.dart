import 'package:flutter/material.dart';

import '../../models/base/episode.dart';

class EpisodeItem extends StatelessWidget {
  final Episode episode;
  final Size size;
  final Function()? onTap;

  const EpisodeItem({
    Key? key,
    required this.episode,
    required this.size,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'episodeIndex_${episode.id}',
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          margin: EdgeInsets.only(bottom: size.height * 0.0049),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              width: size.width - 150,
              padding: const EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    episode.name!,
                    maxLines: 1,
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'SEASON ${episode.season!} EPISODE ${episode.number!}',
                    maxLines: 1,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      episode.airdate != null
                          ? Text(
                              episode.airdate!,
                              style: const TextStyle(
                                fontSize: 16,
                                color: Colors.blue,
                              ),
                            )
                          : Container(),
                      episode.airtime != null
                          ? Text(
                              episode.airtime!,
                              style: const TextStyle(
                                fontSize: 16,
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
