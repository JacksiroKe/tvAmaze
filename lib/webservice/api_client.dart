import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

import '../models/base/episode.dart';
import '../models/base/show.dart';
import '../models/response/schedule_resp.dart';
import '../models/response/show_search_resp.dart';
import '../utils/constants/api_constants.dart';
import 'web_service.dart';

part 'api_client.g.dart';

@dev
@prod
@Singleton(as: WebService)
@RestApi()
abstract class ApiClient extends WebService {
  @factoryMethod
  factory ApiClient(Dio dio) = _ApiClient;

  @override
  @GET(ApiConstants.schedule)
  Future<List<ScheduleResp>> getSchedule();

  @override
  @GET(ApiConstants.showSearch)
  Future<List<ShowSearchResp>> getShowSearch(
    @Query("q") String query,
  );
  
  @override
  @GET('${ApiConstants.shows}{show}')
  Future<Show> getShow(
    @Path("show") int show,
  );

  @override
  @GET('${ApiConstants.shows}{show}${ApiConstants.episodes}')
  Future<List<Episode>> getShowEpisodes(
    @Path("show") int show,
  );
}
